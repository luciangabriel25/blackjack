#pragma once
#include <string>
#include <fstream>
#include <unordered_map>

enum class ConfigurationError
{
	CantOpenFile,
	EmptyFile,
	MalformedEntry
};

struct FileNotFound
{
	std::string message;
	std::string what() {
		return message;
	}
};

class Configuration
{
public:
	Configuration(std::string path);
	~Configuration();
	void printConfiguration() const noexcept;
	int operator[](std::string);
	bool good() const;
	void reload();
private:
	void Load();
	bool m_good;
	std::string m_lastPath;
	std::ifstream input;
	std::unordered_map<std::string, int> settings;
};

