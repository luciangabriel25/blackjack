#pragma once
#include "Hand.h"

enum Action
{
	HIT,
	STAND
};

class Player
{
public:
	Player(const char* name);
	~Player();

	const char* GetName() const;
	Hand& GetHand();

	Action GetAction() const;

private:
	char* m_name;
	Hand m_hand;
};

