#include "stdafx.h"
#include "Any.h"

Any::Any()
	:m_addressOfValue(nullptr)
{

}

Any::~Any()
{
	if (m_addressOfValue != nullptr)
	{
		m_deleter(m_addressOfValue);
	}
}

Any::Any(const Any& other)
{
	other.m_copier(m_addressOfValue, other.m_addressOfValue);
	m_copier = other.m_copier;
	m_deleter = other.m_deleter;
}
